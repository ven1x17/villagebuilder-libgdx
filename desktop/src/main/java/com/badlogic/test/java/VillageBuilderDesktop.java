package com.badlogic.test.java;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import com.badlogic.test.core.Renderer;
import com.badlogic.test.core.util.Settings;

public class VillageBuilderDesktop {
	public static void main (String[] args) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = Settings.w;
		config.height = Settings.h;
		config.backgroundFPS = 20;
		config.foregroundFPS = 60;
		new LwjglApplication(new Renderer(), config);
	}
}
