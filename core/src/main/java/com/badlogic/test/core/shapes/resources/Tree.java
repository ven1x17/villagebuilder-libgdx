package com.badlogic.test.core.shapes.resources;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.test.core.textures.Tex;
import com.badlogic.test.core.textures.TexMapping;
import com.badlogic.test.core.util.Settings;

import static com.badlogic.test.core.util.Settings.TREE_SIZE_TILES;

public class Tree extends ResourceTile {

    public static final Color DARK_GREEN = new Color(0, 0.5f, 0, 1);

    public Tree(int x, int y, Texture texture) {
        super(x, y, TREE_SIZE_TILES, texture);
    }

    @Override
    public Color getMinimapColor() {
        return DARK_GREEN;
    }


}
