package com.badlogic.test.core.shapes.etc;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.test.core.shapes.Tile;

public class FloorTile extends Tile {

    public static final Color LIGHT_GREEN = new Color(0.1f, 0.7f, 0.1f, 1);

    public FloorTile(int x, int y, int size, Texture texture) {
        super(x, y, size, texture);
        setTouchable(Touchable.disabled);
    }

    @Override
    public Color getMinimapColor() {
        return LIGHT_GREEN;
    }

    @Override
    public boolean isPassable() {
        return true;
    }
}
