package com.badlogic.test.core.shapes.resources;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.test.core.util.Settings;
import com.badlogic.test.core.textures.Tex;
import com.badlogic.test.core.textures.TexMapping;

import static com.badlogic.test.core.util.Settings.STONE_SIZE_TILES;

public class Stone extends ResourceTile {
    public Stone(int x, int y) {
        super(x, y, STONE_SIZE_TILES, Tex.get(TexMapping.STONE));
    }

    @Override
    public Color getMinimapColor() {
        return Color.LIGHT_GRAY;
    }
}
