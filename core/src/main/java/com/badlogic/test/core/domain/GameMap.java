package com.badlogic.test.core.domain;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.badlogic.test.core.domain.etc.MapView;
import com.badlogic.test.core.shapes.Tile;
import com.badlogic.test.core.util.Deps;
import com.badlogic.test.core.util.Settings;

public class GameMap extends Stage {

    public final MapView mapView;
    public final float worldPxSizeX;
    public final float worldPxSizeY;
    public final int mapDimensionsX;
    public final int mapDimensionsY;

    public GameMap(int mapDimensionsX, int mapDimensionsY, float tileSize, Viewport viewport, SpriteBatch batch) {
        super(viewport, batch);
        this.mapDimensionsX = mapDimensionsX;
        this.mapDimensionsY = mapDimensionsY;
        mapView = new MapView(mapDimensionsX, mapDimensionsY);
        worldPxSizeX = mapDimensionsX * tileSize;
        worldPxSizeY = mapDimensionsY * tileSize;

        new MapGen(System.currentTimeMillis()).generate(this);

        Deps.getInstance().camera.translate(worldPxSizeX / 2 - Settings.w / 2f, worldPxSizeY / 2 - Settings.h / 2f);
    }

    public void update() {
        act();
        draw();
    }

    @Override
    public void draw() {
        super.draw();
    }

    public void addTile(Tile tile) {
        mapView.declareTile(tile.getMapX(), tile.getMapY(), tile.getSizeInTiles(), tile);
        addActor(tile);
    }
}
