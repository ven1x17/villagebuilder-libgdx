package com.badlogic.test.core.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import java.nio.channels.NotYetBoundException;
import java.util.HashMap;
import java.util.Map;

public class Assets {

    private static final Map<String, BitmapFont> FONTS = new HashMap<String, BitmapFont>();

    public static BitmapFont generateFont(String id, FreeTypeFontGenerator.FreeTypeFontParameter parameter) {
        BitmapFont bitmapFont = FONTS.get(id);
        if (bitmapFont == null) {
            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("truetypefont/NotoSans-Regular.ttf"));
            BitmapFont font = generator.generateFont(parameter);
            generator.dispose();
            FONTS.put(id, font);
            bitmapFont = font;
        }

        return bitmapFont;
    }

}
