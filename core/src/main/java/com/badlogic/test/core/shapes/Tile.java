package com.badlogic.test.core.shapes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Array;
import com.badlogic.test.core.util.Deps;

import static com.badlogic.test.core.util.Settings.TILE_SIZE;


public class Tile extends Actor {
    public static int tileIndex = 0;

    private int mapX;
    private int mapY;
    private int sizeInTiles;
    private Texture texture;
    private boolean touched;
    public int index;

    public Tile(int x, int y, int sizeInTiles, Texture texture) {
        index = tileIndex++;
        mapX = x;
        mapY = y;
        this.sizeInTiles = sizeInTiles;
        this.texture = texture;
        setX(x * TILE_SIZE);
        setY(y * TILE_SIZE);
        setSize(sizeInTiles * TILE_SIZE, sizeInTiles * TILE_SIZE);
        setTouchable(Touchable.enabled);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (touched) {
            Color color = batch.getColor();
            batch.setColor(1, 1, 0, 0.9f);
            batch.draw(texture, getX(), getY(), getWidth(), getHeight());
            batch.setColor(color);
        } else {
            Color color = getColor();
            batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
            batch.draw(texture, getX(), getY(), getWidth(), getHeight());
        }
    }

    @Override
    public boolean isVisible() {
        return Deps.getInstance().viewportUtil.isVisible(getX(), getY(), getWidth());
    }

    public void touch(boolean touched) {
        this.touched = touched;
        Deps.getInstance().hud.globalTooltip.setText(getTooltipText());
    }

    protected String getTooltipText() {
        return Deps.getInstance().dictionary.getLocalizedString(getClass().getSimpleName());
    }

    public boolean isTouched() {
        return touched;
    }

    public void moveTo(Vector2 stageCoords, float speed) {
        clearActions();
        float animationTime = stageCoords.dst(getX(), getY()) / speed;
        addAction(Actions.moveTo(stageCoords.x, stageCoords.y, animationTime));
    }

    public void movementSequence(Array<Tile> sequence, float speed, Vector2 start) {
        clearActions();
        Action[] actions = new Action[sequence.size - 1];
        for (int i = 1; i < sequence.size; i++) {
            float x = sequence.get(i).getX();
            float y = sequence.get(i).getY();
            Vector2 prev = i == 1 ? start : sequence.get(i - 1).coords();
            float animationTime = prev.dst(x, y) / speed;
            actions[i - 1] = Actions.moveTo(x, y, animationTime);
        }
        addAction(Actions.sequence(actions));
    }

    public Vector2 coords() {
        return new Vector2(getX(), getY());
    }

    public Color getMinimapColor() {
        return Color.DARK_GRAY;
    }

    public int getMapX() {
        return mapX;
    }

    public int getMapY() {
        return mapY;
    }

    public int getSizeInTiles() {
        return sizeInTiles;
    }

    public boolean isPassable() {
        return false;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" + mapX + "/" + mapY + '}';
    }
}
