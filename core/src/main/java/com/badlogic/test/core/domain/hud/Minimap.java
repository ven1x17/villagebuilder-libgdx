package com.badlogic.test.core.domain.hud;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.test.core.shapes.Tile;
import com.badlogic.test.core.util.Deps;
import com.badlogic.test.core.util.Settings;
import com.badlogic.test.core.util.hack.ViewportUtil;

public class Minimap extends Actor {

    public static final float ZOOM_LEVEL = 2f;
    public final int mapX = 1;
    public final int mapY = 1;
    private Deps deps;

    public Minimap() {
        this.deps = Deps.getInstance();
        setWidth(z(Settings.MAP_SIZE));
        setHeight(z(Settings.MAP_SIZE));
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        ShapeRenderer sr = deps.shape;
        sr.setProjectionMatrix(deps.hud.getCamera().combined);

        drawOutline();
        drawMiniMapObjects();
        drawViewportRectangle();
    }

    private void drawOutline() {
        deps.shape.begin(ShapeRenderer.ShapeType.Line);
        deps.shape.setColor(Color.BLACK);
        deps.shape.rect(mapX, mapY, getWidth() + 1, getHeight() + 1);
        deps.shape.end();
    }

    private void drawMiniMapObjects() {
        // TODO: this can be cached
        deps.shape.begin(ShapeRenderer.ShapeType.Filled);
        for (int i = 0; i < deps.map.getActors().items.length; i++) {
            Tile a = (Tile) deps.map.getActors().items[i];
            if (a == null || a.getMinimapColor().a == 0) {
                continue;
            }
            deps.shape.setColor(a.getMinimapColor());
            deps.shape.rect(z(mapX + (a.getX() / Settings.TILE_SIZE)),
                    z(mapY + (a.getY() / Settings.TILE_SIZE)),
                    z(a.getWidth() / Settings.TILE_SIZE),
                    z(a.getHeight() / Settings.TILE_SIZE));
        }
        deps.shape.end();
    }

    private void drawViewportRectangle() {
        deps.shape.begin(ShapeRenderer.ShapeType.Line);
        deps.shape.setColor(Color.WHITE);
        deps.shape.rect(
                deps.camera.zoom * (z(mapX + (deps.viewportUtil.getX() / Settings.TILE_SIZE))),
                deps.camera.zoom * (z(mapY + (deps.viewportUtil.getY() / Settings.TILE_SIZE))),
                deps.camera.zoom * (z(Settings.w / (float) Settings.TILE_SIZE)),
                deps.camera.zoom * (z(Settings.h / (float) Settings.TILE_SIZE))
        );
        deps.shape.end();
    }

    private float z(float toZoom) {
        return ZOOM_LEVEL * toZoom;
    }
}
