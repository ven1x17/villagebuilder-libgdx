package com.badlogic.test.core.domain.etc.pathfinding;

import com.badlogic.gdx.ai.pfa.DefaultGraphPath;
import com.badlogic.gdx.ai.pfa.SmoothableGraphPath;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.test.core.shapes.Tile;

public class TiledSmoothableGraphPath extends DefaultGraphPath<Tile> implements
        SmoothableGraphPath<Tile, Vector2> {

    private Vector2 tmpPosition = new Vector2();

    public TiledSmoothableGraphPath(int mapSize) {
        super(mapSize);
    }

    @Override
    public Vector2 getNodePosition(int index) {
        Tile node = nodes.get(index);
        return tmpPosition.set(node.getMapX(), node.getMapY());
    }

    @Override
    public void swapNodes(int index1, int index2) {
        nodes.set(index1, nodes.get(index2));
    }

    @Override
    public void truncatePath(int newLength) {
        nodes.truncate(newLength);
    }
}
