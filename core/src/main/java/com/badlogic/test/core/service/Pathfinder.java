package com.badlogic.test.core.service;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.Heuristic;
import com.badlogic.gdx.ai.pfa.PathSmoother;
import com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder;
import com.badlogic.gdx.ai.pfa.indexed.IndexedHierarchicalGraph;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.test.core.domain.etc.pathfinding.TiledSmoothableGraphPath;
import com.badlogic.test.core.shapes.Tile;
import com.badlogic.test.core.util.Deps;
import com.badlogic.test.core.util.Settings;
import com.badlogic.test.core.util.TiledRaycastCollisionDetector;

import java.util.HashMap;
import java.util.Map;

public class Pathfinder {

    private final IndexedAStarPathFinder<Tile> pf;
    private final IndexedHierarchicalGraph<Tile> graph;
    private final PathSmoother pathSmoother;

    public Pathfinder() {
        graph = new IndexedHierarchicalGraph<Tile>(1) {
            Map<Integer, Array<Connection<Tile>>> cache = new HashMap<Integer, Array<Connection<Tile>>>();
            @Override
            public Tile convertNodeBetweenLevels(int inputLevel, Tile node, int outputLevel) {
                return node;
            }

            @Override
            public int getIndex(Tile node) {
                return node.index;
            }

            @Override
            public int getNodeCount() {
                return Settings.MAP_SIZE * Settings.MAP_SIZE;
            }

            @Override
            public Array<Connection<Tile>> getConnections(Tile fromNode) {
                Array<Connection<Tile>> connections = cache.get(fromNode.index);
                if (connections == null) {
                    connections = Deps.getInstance().map.mapView.getConnections(fromNode);
                    cache.put(fromNode.index, connections);
                }
                return connections;
            }
        };
        pf = new IndexedAStarPathFinder<Tile>(graph);
        pathSmoother = new PathSmoother<Tile, Vector2>(new TiledRaycastCollisionDetector());
    }

    public Array<Tile> findPath(Tile fromNode, Tile toNode) {
        TiledSmoothableGraphPath outPath = new TiledSmoothableGraphPath(Settings.MAP_SIZE);
        pf.searchNodePath(fromNode, toNode, new Heuristic<Tile>() {
            @Override
            public float estimate(Tile node, Tile endNode) {
                return new Vector2(node.getMapX(), node.getMapY()).dst(endNode.getMapX(), endNode.getMapY());
            }
        }, outPath);
        System.out.print("Start: " + fromNode);
        for (int i = 0; i < outPath.getCount(); i++) {
            Tile next = outPath.get(i);
            System.out.print("-->" + next);
        }
        System.out.println("==> Finish: " + toNode);

        pathSmoother.smoothPath(outPath);
        return outPath.nodes;
    }

    private Array<Tile> smooth(Array<Tile> outPath) {
        for (int i = 0; i < outPath.size; i++) {
            Tile start = outPath.get(i);
            for (int j = outPath.size - 1; j >= 0; j--) {
                Tile end = outPath.get(j);
                if (haveDirectPath(start, end)) {

                }
            }
        }
        return outPath;
    }

    private boolean haveDirectPath(Tile start, Tile end) {

        return false;
    }

}
