package com.badlogic.test.core.domain;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.test.core.domain.etc.Mouse;
import com.badlogic.test.core.domain.hud.Minimap;
import com.badlogic.test.core.shapes.Tile;
import com.badlogic.test.core.util.Deps;

import static com.badlogic.test.core.util.Settings.*;

public class InputListener implements InputProcessor {

    private Tile lastTouched;
    private Tile touchStartSelection;
    private Deps deps;

    public InputListener() {
        this.deps = Deps.getInstance();
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        if (character == 'r') {
            deps.init();
        }
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Mouse.touch(button, screenX, screenY);

        if (Mouse.PRIMARY == button) {
            if (handleHudHit(screenX, screenY, button)) {
                return true;
            }

            Vector2 stageCoords = getMapCoordsFromViewCoords(screenX, screenY);

            Actor hit = deps.map.hit(stageCoords.x, stageCoords.y, true);
            if (hit instanceof Tile) {
                touchStartSelection = (Tile) hit;
            }
        }
        if (Mouse.MIDDLE == button) {
//            System.out.println("panning cam @ " + screenX + " / " + screenY);
        }
        if (Mouse.RIGHT == button && deps.mainActor.isTouched()) {

            Vector2 mapCoordsFromViewCoords = getMapCoordsFromViewCoords(screenX, screenY);
            Tile target = (Tile) deps.map.hit(mapCoordsFromViewCoords.x, mapCoordsFromViewCoords.y, false);
            if (target != null && target.isPassable()) {
                Array<Tile> path = deps.pathfinder.findPath(deps.map.mapView.hit(deps.mainActor.getMapX(), deps.mainActor.getMapY()), target);
                if (path.size == 0) {
                    System.out.println("Unable to find path");
                } else {
                    deps.mainActor.movementSequence(path);
                }
            }
        }
        return true;
    }

    private boolean handleHudHit(float screenX, float screenY, int button) {
        Vector2 hudCoords = deps.hud.screenToStageCoordinates(new Vector2(screenX, screenY));
        Actor hudHit = deps.hud.hit(hudCoords.x, hudCoords.y, false);

        if (hudHit != null) {
            System.out.println(hudCoords + " Hud hit " + hudHit.getClass().getSimpleName());
            if (hudHit instanceof Minimap) {
                if (Mouse.PRIMARY == button) {
                    moveCamToMinimapPos(hudCoords.x, hudCoords.y);
                }
            }
            return true;
        }
        return false;
    }

    private void moveCamToMinimapPos(float x, float y) {
        OrthographicCamera cam = deps.camera;
        cam.position.x = x * TILE_SIZE / Minimap.ZOOM_LEVEL - deps.hud.minimap.mapX;
        cam.position.y = y * TILE_SIZE / Minimap.ZOOM_LEVEL - deps.hud.minimap.mapY;
    }

    private Vector2 getMapCoordsFromViewCoords(int screenX, int screenY) {
        return deps.map.screenToStageCoordinates(new Vector2(screenX, screenY));
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (Mouse.MIDDLE == Mouse.getButtonPressed()) {
            OrthographicCamera cam = deps.camera;
            cam.translate(Mouse.getButtonStartX() - screenX, screenY - Mouse.getButtonStartY());

            cam.position.x = Deps.getInstance().viewportUtil.clampCamWidth(cam.position.x, deps.map.worldPxSizeX);
            cam.position.y = Deps.getInstance().viewportUtil.clampCamHeight(cam.position.y, deps.map.worldPxSizeY);

            Mouse.touch(Mouse.MIDDLE, screenX, screenY);
        }

        if (Mouse.PRIMARY == Mouse.getButtonPressed()) {
            if (handleHudHit(screenX, screenY, Mouse.PRIMARY)) {
                return true;
            }
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (2 == button) {
//            System.out.println("stop panning cam @ " + screenX + " / " + screenY);
        }
        Mouse.touchUp();
        if (touchStartSelection != null) {
            if (lastTouched != null) {
                lastTouched.touch(false);
            }
            Vector2 stageCoords = getMapCoordsFromViewCoords(screenX, screenY);
            Actor hit = deps.map.hit(stageCoords.x, stageCoords.y, true);
            if (hit == touchStartSelection) {
                lastTouched = (Tile) touchStartSelection;
                lastTouched.touch(true);
                deps.selectionOutline.setSelected(lastTouched);
            }
            touchStartSelection = null;
        }
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        deps.camera.zoom = MathUtils.clamp(deps.camera.zoom += (float) amount / 5, ZOOM_FACTOR_MIN, ZOOM_FACTOR_MAX);
        return true;
    }
}
