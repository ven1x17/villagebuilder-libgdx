package com.badlogic.test.core.shapes.resources;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.test.core.shapes.Tile;

public abstract class ResourceTile extends Tile {
    public ResourceTile(int x, int y, int size, Texture texture) {
        super(x, y, size, texture);
        setTouchable(Touchable.enabled);
    }

    @Override
    public Color getMinimapColor() {
        return Color.WHITE;
    }
}
