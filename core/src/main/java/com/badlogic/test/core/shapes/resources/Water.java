package com.badlogic.test.core.shapes.resources;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.test.core.domain.MapCoordinates;
import com.badlogic.test.core.textures.Tex;
import com.badlogic.test.core.textures.TexMapping;
import com.badlogic.test.core.util.Settings;
import com.google.common.base.Function;

public class Water extends ResourceTile {
    public static final Function<MapCoordinates, Water> WATER_GENERATOR = new Function<MapCoordinates, Water>() {
        @Override
        public Water apply(MapCoordinates mapCoordinates) {
            return new Water(mapCoordinates.x, mapCoordinates.y, Tex.get(TexMapping.LAKE_WATER));
        }
    };

    public Water(int x, int y, Texture texture) {
        super(x, y, Settings.WATER_SIZE_TILES, texture);
    }

    @Override
    public Color getMinimapColor() {
        return Color.BLUE;
    }
}
