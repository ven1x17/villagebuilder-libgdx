package com.badlogic.test.core.domain;

public class MapCoordinates {
    public int x;
    public int y;

    public MapCoordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
