package com.badlogic.test.core.domain;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.test.core.domain.etc.BrushSize;
import com.badlogic.test.core.shapes.Tile;
import com.badlogic.test.core.shapes.etc.FloorTile;
import com.badlogic.test.core.shapes.resources.Stone;
import com.badlogic.test.core.shapes.resources.Tree;
import com.badlogic.test.core.shapes.resources.Water;
import com.badlogic.test.core.textures.Tex;
import com.badlogic.test.core.textures.TexMapping;
import com.google.common.base.Function;

import java.util.Random;

import static com.badlogic.test.core.util.Settings.*;

public class MapGen {

    public static final int MAX_RANDOM_SPIN_COUNTER = 200;
    public final Random random;

    public MapGen(long currentTimeMillis) {
        random = new Random(currentTimeMillis);
    }

    public void generate(GameMap gameMap) {
        generateLakes(gameMap);
        generateForests(gameMap);
        generateStones(gameMap);
        generateFloor(gameMap, gameMap.mapDimensionsX, gameMap.mapDimensionsY);
    }

    private void generateFloor(GameMap gameMap, int mapDimensionsX, int mapDimensionsY) {
        for (int x = 0; x < mapDimensionsX; x++) {
            for (int y = 0; y < mapDimensionsY; y++) {
                if (!gameMap.mapView.intersects(x, y, 1)) {
                    gameMap.addTile(new FloorTile(x, y, 1, Tex.getRandomGroundTex()));
                }
            }
        }
    }

    private void generateLakes(GameMap gameMap) {
        for (int i = 0; i < 1 + random.nextInt(LAKES_COUNT_MAX - 1); i++) {
            MapCoordinates nextLocation = getNewEmptyLocation(WATER_SIZE_TILES, gameMap);
            int lakeSize = LAKE_SIZE_TILES_MIN + random.nextInt(LAKE_SIZE_TILES_MAX - LAKE_SIZE_TILES_MIN);
            for (int j = 0; j < lakeSize; j++) {
                brushAdd(nextLocation.x, nextLocation.y, BrushSize.THREE_BY_THREE, gameMap, Water.WATER_GENERATOR);
                nextLocation = getLocationNearby(nextLocation.x, nextLocation.y, WATER_SIZE_TILES, gameMap);
                if (nextLocation == null) {
                    System.out.println("Creating another lake cause old one is stuck");
                    nextLocation = getNewEmptyLocation(WATER_SIZE_TILES, gameMap);
                }
            }
        }
        while (fillWaterInsideLakes(gameMap) > 0) {
            System.out.println("Filling water");
        };

    }

    private int fillWaterInsideLakes(GameMap gameMap) {
        int count = 0;
        Tile[][] map = gameMap.mapView.map;
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (map[i][j] == null) {
                    int surroundedBy = gameMap.mapView.getSurroundedBy(i, j, Water.class);
                    if (surroundedBy >= 3) {
                        gameMap.addTile(new Water(i,j, Tex.get(TexMapping.LAKE_WATER)));
                        count++;
                    }
                }
            }
        }
        return count;
    }

    private void brushAdd(int x, int y, BrushSize brushSize, GameMap gameMap, Function<MapCoordinates, Water> func) {
        for (int i = -brushSize.size; i <= brushSize.size; i++) {
            for (int j = -brushSize.size; j <= brushSize.size; j++) {
                int newX = x + i;
                int newY = y + j;
                if (newX < 0 || newY < 0 || newX >= gameMap.mapDimensionsX || newY >= gameMap.mapDimensionsY) {
                    continue;
                }
                if (!gameMap.mapView.intersects(newX, newY, 1)) {
                    gameMap.addTile(func.apply(new MapCoordinates(newX, newY)));
                }
            }
        }
    }

    private void generateStones(GameMap gameMap) {
        for (int i = 0; i < STONES_COUNT; i++) {
            MapCoordinates loc = getNewEmptyLocation(STONE_SIZE_TILES, gameMap);
            gameMap.addTile(new Stone(loc.x, loc.y));
        }
    }

    private void generateForests(GameMap gameMap) {
        for (int i = 0; i < FORESTS_COUNT; i++) {
            MapCoordinates nextLocation = getNewEmptyLocation(TREE_SIZE_TILES, gameMap);
            Texture treeTex;
            switch (random.nextInt(3)) {
                case 0:
                    treeTex = Tex.get(TexMapping.TREE_APPLE);
                    break;
                case 1:
                    treeTex = Tex.get(TexMapping.TREE_OAK);
                    break;
                default:
                    treeTex = Tex.get(TexMapping.TREE_PINE);
                    break;
            }
            for (int j = 0; j < FOREST_SIZE_TREES; j++) {
                gameMap.addTile(new Tree(nextLocation.x, nextLocation.y, treeTex));
                nextLocation = getLocationNearby(nextLocation.x, nextLocation.y, TREE_SIZE_TILES, gameMap);
                if (nextLocation == null) {
                    System.out.println("Creating another forest cause old one is stuck");
                    nextLocation = getNewEmptyLocation(TREE_SIZE_TILES, gameMap);
                }
            }
        }
    }

    private MapCoordinates getLocationNearby(int x, int y, int objectSizeTiles, GameMap gameMap) {
        int newX = x, newY = y;
        for (int i = 0; i < MAX_RANDOM_SPIN_COUNTER; i++) {
            switch (random.nextInt(4)) {
                case 0:
                    newX = newX - objectSizeTiles <= 0 ? newX + objectSizeTiles : newX - objectSizeTiles;
                    break;
                case 1:
                    newY = newY - objectSizeTiles <= 0 ? newY + objectSizeTiles : newY - objectSizeTiles;
                    break;
                case 2:
                    newX = newX + objectSizeTiles >= MAP_SIZE ? newX - objectSizeTiles : newX + objectSizeTiles;
                    break;
                case 3:
                    newY = newY + objectSizeTiles >= MAP_SIZE ? newY - objectSizeTiles : newY + objectSizeTiles;
                    break;
            }

            if (!intersectsAnotherTile(newX, newY, objectSizeTiles, gameMap)) {
                return new MapCoordinates(newX, newY);
            }
        }
        System.out.println("Can't get location nearby for object <" + x + " / " + y + " / " + objectSizeTiles + ">");
        return null;
    }

    private boolean intersectsAnotherTile(int newX, int newY, int objectSizeTiles, GameMap gameMap) {
        return gameMap.mapView.intersects(newX, newY, objectSizeTiles);
    }

    private int getRandomMapCoordFor(int entitiSizeInTiles) {
        int randomLocation = random.nextInt(MAP_SIZE);
        return Math.min(randomLocation, MAP_SIZE - entitiSizeInTiles - 1);
    }

    private MapCoordinates getNewEmptyLocation(int entitiSizeInTiles, GameMap gameMap) {
        int x = Integer.MIN_VALUE;
        int y = Integer.MIN_VALUE;

        int spinProtector = MAX_RANDOM_SPIN_COUNTER;

        while (x == Integer.MIN_VALUE || intersectsAnotherTile(x, y, entitiSizeInTiles, gameMap)) {
            x = getRandomMapCoordFor(entitiSizeInTiles);
            y = getRandomMapCoordFor(entitiSizeInTiles);
            if (spinProtector-- < 0) {
                throw new IllegalStateException("Spin too much");
            }
        }
        return new MapCoordinates(x, y);
    }
}
