package com.badlogic.test.core.textures;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

import java.util.EnumMap;
import java.util.Random;

public class Tex {

    private static final Random RANDOM = new Random();

    public static EnumMap<TexMapping, Texture> textures = new EnumMap<TexMapping, Texture>(TexMapping.class);

    public static void init() {
        for (TexMapping value : TexMapping.values()) {
            textures.put(value, new Texture(Gdx.files.internal(value.path)));
        }
    }

    public static Texture get(TexMapping mapping) {
        return textures.get(mapping);
    }

    public static Texture getRandomGroundTex() {
        int r = RANDOM.nextInt(100);
        if (r < 4) {
            return textures.get(TexMapping.GROUND_4);
        }
        if (r < 10) {
            return textures.get(TexMapping.GROUND_1);
        }
        if (r < 17) {
            return textures.get(TexMapping.GROUND_1);
        }
        if (r < 35) {
            return textures.get(TexMapping.GROUND_2);
        }
        if (r < 50) {
            return textures.get(TexMapping.GROUND_BASE);
        }
        return textures.get(TexMapping.GROUND_0);
    }
}
