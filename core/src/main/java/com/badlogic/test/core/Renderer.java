package com.badlogic.test.core;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.test.core.textures.Tex;
import com.badlogic.test.core.util.Deps;
import com.badlogic.test.core.util.Settings;

public class Renderer implements ApplicationListener {

    private Deps deps;

    @Override
    public void create() {
        Tex.init();
        deps = new Deps();

        InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(deps.inputListener);
        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
//        Gdx.gl.glEnable(GL30.GL_BLEND);
//        Gdx.gl.glBlendFunc(GL30.GL_SRC_ALPHA,GL30.GL_ONE_MINUS_SRC_ALPHA);
//        batch.enableBlending();
        deps.map.update();
        deps.selectionOutline.draw();
        deps.hud.update();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }

    @Override
    public void resize(int width, int height) {
        Settings.h = height;
        Settings.w = width;
    }
}
