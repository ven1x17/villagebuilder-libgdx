package com.badlogic.test.core.util;

public class Settings {
    public static int w = 1600;
    public static int h = 900;

    public static final int MAP_SIZE = 128;
    public static final int TILE_SIZE = 32;

    public static final float ZOOM_FACTOR_MIN = 0.25f;
    public static final float ZOOM_FACTOR_MAX = 2;

    public static final int STONE_SIZE_TILES = 4;
    public static final int TREE_SIZE_TILES = 1;
    public static final int WATER_SIZE_TILES = 1;

    public static final int MAIN_ACTOR_SIZE = 2;
    public static final int MAIN_ACTOR_SIZE_PX = Settings.TILE_SIZE * 2;
    public static final float MAIN_ACTOR_MOVE_TARGET_CROSS_SIZE = 0.8f;
    public static final int MAIN_ACTOR_SPEED_PER_SEC = 256;

    // map gen
    public static final int STONES_COUNT = 8;
    public static final int FORESTS_COUNT = 8;
    public static final int FOREST_SIZE_TREES = 300;
    public static final int LAKES_COUNT_MAX = 4;
    public static final int LAKE_SIZE_TILES_MIN = 30;
    public static final int LAKE_SIZE_TILES_MAX = 150;
}
