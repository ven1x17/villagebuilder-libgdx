package com.badlogic.test.core.domain.etc;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.DefaultConnection;
import com.badlogic.gdx.utils.Array;
import com.badlogic.test.core.shapes.Tile;

public class MapView {

    public final Tile[][] map;
    private int width;
    private int height;

    public MapView(int mapDimensionsX, int mapDimensionsY) {
        this.width = mapDimensionsX;
        this.height = mapDimensionsY;
        map = new Tile[mapDimensionsX][mapDimensionsY];
    }

    public void declareTile(int x, int y, int size, Tile tile) {
        if (size == 1) {
            map[x][y] = tile;
            return;
        }
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (map[x + i][y + j] != null) {
                    throw new IllegalStateException("Map generation failed, tile already occupied by " + map[x + i][y + i]);
                }
                map[x + i][y + j] = tile;
            }
        }
    }

    public Tile hit(int x, int y) {
        return map[x][y];
    }

    public boolean intersects(int x, int y, int size) {
        if (size == 1) {
            if (x > width || y > map[x].length) {
                return false;
            }
            return hit(x, y) != null;
        }
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (x + i >= width || y + j >= height) {
                    continue;
                }
                if (map[x + i][y + j] != null) {
                    return true;
                }
            }
        }
        return false;
    }

    public int getSurroundedBy(int i, int j, Class<? extends Tile> surrounding) {
        int surroundCount = 0;
        if (i - 1 > 0 && map[i - 1][j] != null && map[i - 1][j].getClass().equals(surrounding)) {
            surroundCount++;
        }
        if (i + 1 < width && map[i + 1][j] != null && map[i + 1][j].getClass().equals(surrounding)) {
            surroundCount++;
        }
        if (j - 1 > 0 && map[i][j - 1] != null && map[i][j - 1].getClass().equals(surrounding)) {
            surroundCount++;
        }
        if (j + 1 < width && map[i][j + 1] != null && map[i][j + 1].getClass().equals(surrounding)) {
            surroundCount++;
        }
        return surroundCount;
    }

    public Array<Connection<Tile>> getConnections(Tile fromNode) {
        Array<Connection<Tile>> connections = new Array<Connection<Tile>>(8);
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if (i == 0 && j == 0) {
                    continue;
                }
                int newX = fromNode.getMapX() + i;
                int newY = fromNode.getMapY() + j;
                if (newX < 0 || newX >= width || newY < 0 || newY >= height) {
                    continue;
                }
                Tile toNode = map[newX][newY];
                if (toNode != null && toNode.isPassable()) {
                    connections.add(new DefaultConnection<Tile>(fromNode, toNode));
                }
            }
        }
        return connections;
    }
}
