package com.badlogic.test.core.domain;

public class GenerationFailedException extends RuntimeException {
    public GenerationFailedException(String s) {
        super(s);
    }
}
