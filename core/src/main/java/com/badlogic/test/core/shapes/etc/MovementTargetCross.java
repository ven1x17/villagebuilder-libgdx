package com.badlogic.test.core.shapes.etc;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.test.core.shapes.Tile;
import com.badlogic.test.core.textures.Tex;
import com.badlogic.test.core.textures.TexMapping;
import com.badlogic.test.core.util.Deps;

import static com.badlogic.test.core.util.Settings.MAIN_ACTOR_MOVE_TARGET_CROSS_SIZE;
import static com.badlogic.test.core.util.Settings.TILE_SIZE;

public class MovementTargetCross extends Tile {
    public static final float HALF_TARGET_CROSS_SIZE_PX = MAIN_ACTOR_MOVE_TARGET_CROSS_SIZE / 2f * TILE_SIZE;
    public static final Color TRANSPARENT = new Color(0, 0, 0, 0);
    private float destX;
    private float destY;

    public MovementTargetCross() {
        super(0, 0, 1, Tex.get(TexMapping.MOVEMENT_TARGET_CROSS));
        setSize(MAIN_ACTOR_MOVE_TARGET_CROSS_SIZE * TILE_SIZE, MAIN_ACTOR_MOVE_TARGET_CROSS_SIZE * TILE_SIZE);
        setTouchable(Touchable.disabled);
    }

    @Override
    public Color getMinimapColor() {
        return destX == Deps.getInstance().mainActor.getX() && destY == Deps.getInstance().mainActor.getY() ? TRANSPARENT : Color.YELLOW;
    }

    public void setDestination(float clickedX, float clickedY, Vector2 destination) {
        this.destX = destination.x;
        this.destY = destination.y;
        setX(clickedX - HALF_TARGET_CROSS_SIZE_PX);
        setY(clickedY - HALF_TARGET_CROSS_SIZE_PX);
        clearActions();
        addAction(Actions.sequence(Actions.alpha(1), Actions.fadeOut(0.3f)));
    }
}
