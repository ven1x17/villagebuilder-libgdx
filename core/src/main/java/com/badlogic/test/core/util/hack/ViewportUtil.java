package com.badlogic.test.core.util.hack;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.test.core.util.Deps;
import com.badlogic.test.core.util.Settings;

public class ViewportUtil {

    public float getX() {
        return z(Deps.getInstance().camera.position.x) - Settings.w / 2f;
    }

    public float getY() {
        return z(Deps.getInstance().camera.position.y) - Settings.h / 2f;
    }

    public boolean isVisible(float x, float y, float size) {
        if (getX() - z(size) <= z(x) && z(x + size) <= getX() + z(size) + Settings.w &&
                getY() - z(size) <= z(y) && z(y + size) <= getY() + z(size) + Settings.h) {
            return true;
        }
        return false;
    }

    public float z(float v) {
        return v / Deps.getInstance().camera.zoom;
    }

    public float clampCamWidth(float camPosX, float worldSize) {
        return MathUtils.clamp(camPosX, Deps.getInstance().camera.zoom * (Settings.w / 2f) - 100, Deps.getInstance().camera.zoom * (Settings.w / 2f - Settings.w) + worldSize + 100);
    }

    public float clampCamHeight(float camPosY, float worldSize) {
        return MathUtils.clamp(camPosY, Deps.getInstance().camera.zoom * (Settings.h / 2f) - 100, Deps.getInstance().camera.zoom * (Settings.h / 2f - Settings.h) + worldSize + 100);
    }
}
