package com.badlogic.test.core.domain.etc;

public enum BrushSize {
    THREE_BY_THREE(1), FIVE(2);

    public final int size;

    BrushSize(int size) {
        this.size = size;
    }
}
