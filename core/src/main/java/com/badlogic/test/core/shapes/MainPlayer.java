package com.badlogic.test.core.shapes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.test.core.textures.Tex;
import com.badlogic.test.core.textures.TexMapping;
import com.badlogic.test.core.util.Deps;
import com.badlogic.test.core.util.Settings;

import static com.badlogic.test.core.util.Settings.MAIN_ACTOR_SIZE_PX;
import static com.badlogic.test.core.util.Settings.MAIN_ACTOR_SPEED_PER_SEC;

public class MainPlayer extends Tile {

    public static final int HALF_MAIN_ACTOR_SIZE = MAIN_ACTOR_SIZE_PX / 2;
    public static final int QUARTER_MAIN_ACTOR_SIZE = MAIN_ACTOR_SIZE_PX / 4;

    public MainPlayer(int x, int y, int size) {
        super(x, y, size, Tex.get(TexMapping.MAIN_ACTOR));
    }

    public void moveTo(Vector2 stageCoords) {
        Vector2 adjustedStageCoords = new Vector2(stageCoords.x - HALF_MAIN_ACTOR_SIZE, stageCoords.y - QUARTER_MAIN_ACTOR_SIZE);
        Deps.getInstance().mainActorMoveTargetCross.setDestination(stageCoords.x, stageCoords.y, adjustedStageCoords);
        moveTo(adjustedStageCoords, MAIN_ACTOR_SPEED_PER_SEC);
    }

    public void movementSequence(Array<Tile> sequence) {
        if (sequence.size == 0) {
            System.out.println("Can't move there");
            return;
        }
        Tile dest = sequence.get(sequence.size - 1);
        Vector2 adjustedStageCoords = new Vector2(dest.getX() - HALF_MAIN_ACTOR_SIZE, dest.getY() - QUARTER_MAIN_ACTOR_SIZE);
        Deps.getInstance().mainActorMoveTargetCross.setDestination(dest.getX(), dest.getY(), adjustedStageCoords);
        movementSequence(sequence, MAIN_ACTOR_SPEED_PER_SEC, coords());
    }

    @Override
    public Vector2 coords() {
        return new Vector2(getX() - HALF_MAIN_ACTOR_SIZE, getY() - QUARTER_MAIN_ACTOR_SIZE);
    }

    @Override
    public int getMapX() {
        return Math.round((getX() + HALF_MAIN_ACTOR_SIZE) / (float) Settings.TILE_SIZE);
    }

    @Override
    public int getMapY() {
        return Math.round((getY() + HALF_MAIN_ACTOR_SIZE) / (float) Settings.TILE_SIZE);
    }

    @Override
    public Color getMinimapColor() {
        return Color.RED;
    }

    @Override
    protected String getTooltipText() {
        return "Your dude";
    }
}
