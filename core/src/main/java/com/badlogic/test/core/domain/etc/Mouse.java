package com.badlogic.test.core.domain.etc;

public final class Mouse {
    public static final int PRIMARY = 0;
    public static final int RIGHT = 1;
    public static final int MIDDLE = 2;

    private static int buttonPressed = -1;
    private static int buttonStartX;
    private static int buttonStartY;

    public static void touch(int button, int screenX, int screenY) {
        buttonPressed = button;
        buttonStartX = screenX;
        buttonStartY = screenY;
    }

    public static int getButtonPressed() {
        return buttonPressed;
    }

    public static int getButtonStartX() {
        return buttonStartX;
    }

    public static int getButtonStartY() {
        return buttonStartY;
    }

    public static void touchUp() {
        buttonPressed = -1;
    }
}
