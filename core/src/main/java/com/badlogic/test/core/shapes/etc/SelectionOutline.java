package com.badlogic.test.core.shapes.etc;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.test.core.shapes.Tile;
import com.badlogic.test.core.util.Deps;

public class SelectionOutline {

    private Tile lastTouched;

    public void setSelected(Tile lastTouched) {
        this.lastTouched = lastTouched;
    }

    public void draw() {
        if (lastTouched != null) {
            ShapeRenderer sr = Deps.getInstance().shape;
            sr.setProjectionMatrix(Deps.getInstance().camera.combined);
            sr.begin(ShapeRenderer.ShapeType.Line);
//            sr.setColor(new Color(1,1,1,1));
            sr.setColor(Color.BLACK);
            sr.rect(lastTouched.getX(), lastTouched.getY(), lastTouched.getWidth(), lastTouched.getHeight());
            sr.end();
        }
    }

}
