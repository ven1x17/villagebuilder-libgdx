package com.badlogic.test.core.util;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.badlogic.test.core.domain.GameMap;
import com.badlogic.test.core.domain.InputListener;
import com.badlogic.test.core.domain.hud.Hud;
import com.badlogic.test.core.service.Pathfinder;
import com.badlogic.test.core.shapes.MainPlayer;
import com.badlogic.test.core.shapes.Tile;
import com.badlogic.test.core.shapes.etc.MovementTargetCross;
import com.badlogic.test.core.shapes.etc.SelectionOutline;
import com.badlogic.test.core.util.hack.ViewportUtil;

import static com.badlogic.test.core.util.Settings.*;

public class Deps {

    private static Deps INSTANCE;

    public ShapeRenderer shape;
    public SpriteBatch batch;
    public InputListener inputListener;
    public GameMap map;
    public OrthographicCamera camera;
    public Hud hud;
    public Viewport viewport;
    public MainPlayer mainActor;
    public MovementTargetCross mainActorMoveTargetCross;
    public SelectionOutline selectionOutline;
    public ViewportUtil viewportUtil;
    public Dictionary dictionary;
    public Pathfinder pathfinder;

    public Deps() {
        INSTANCE = this;
        init();
    }

    public void init() {
        Tile.tileIndex = 0;
        camera = new OrthographicCamera(Settings.w, Settings.h);
        viewport = new ScreenViewport(camera);
        viewportUtil = new ViewportUtil();
        shape = new ShapeRenderer();
        batch = new SpriteBatch();
        hud = new Hud(batch);
        inputListener = new InputListener();
        map = new GameMap(MAP_SIZE, MAP_SIZE, TILE_SIZE, viewport, batch);
        int middle = MAP_SIZE / 2;
        mainActor = new MainPlayer(middle, middle, MAIN_ACTOR_SIZE);
        map.addActor(mainActor);

        mainActorMoveTargetCross = new MovementTargetCross();
        mainActorMoveTargetCross.addAction(Actions.alpha(0));
        map.addActor(mainActorMoveTargetCross);

        selectionOutline = new SelectionOutline();

        dictionary = new Dictionary();
        pathfinder = new Pathfinder();
    }

    public static Deps getInstance() {
        return INSTANCE;
    }
}
