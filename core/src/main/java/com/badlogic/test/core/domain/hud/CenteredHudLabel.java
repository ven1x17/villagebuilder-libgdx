package com.badlogic.test.core.domain.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.test.core.util.Assets;

public class CenteredHudLabel extends Label {

    private BitmapFont font;
    private float x;
    private float y;

    public CenteredHudLabel(String text, int size, float x, float y) {
        super(text, getLabelStyle(Color.WHITE, size));
        this.x = x;
        this.y = y;
        setSize(getPrefWidth(), getPrefHeight());
        setPosition(x, y);
    }

    @Override
    public void setText(CharSequence newText) {
        super.setText(newText);
        centerAtPosition();
    }

    public void centerAtPosition() {
        setPosition(x - getPrefWidth() / 2f, y);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }

    private static Label.LabelStyle getLabelStyle(Color color, int size) {
        Label.LabelStyle labelStyle = new Label.LabelStyle();

        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = size;
        parameter.borderWidth = 1;
//        parameter.color = Color.BLACK;
//        parameter.shadowOffsetX = 1;
//        parameter.shadowOffsetY = 1;
//        parameter.shadowColor = new Color(0, 0.5f, 0, 0.75f);
//        parameter.shadowColor = Color.GRAY;

        labelStyle.font = Assets.generateFont(color.toString() + size, parameter);
        labelStyle.fontColor = color;
        return labelStyle;
    }
}
