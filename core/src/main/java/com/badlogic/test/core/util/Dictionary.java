package com.badlogic.test.core.util;

import com.badlogic.test.core.shapes.MainPlayer;
import com.badlogic.test.core.shapes.resources.Stone;
import com.badlogic.test.core.shapes.resources.Tree;
import com.badlogic.test.core.shapes.resources.Water;

import java.util.HashMap;
import java.util.Map;

public class Dictionary {
    public static final String DEFAULT = "<Guess what it is>";

    private static final Map<String, String> en_US = new HashMap<String, String>() {{
        put(MainPlayer.class.getSimpleName(), "Your dude");
        put(Tree.class.getSimpleName(), "A tree");
        put(Stone.class.getSimpleName(), "A stone");
        put(Water.class.getSimpleName(), "Water");
    }};

    public String getLocalizedString(String key) {
        String s = en_US.get(key);
        return s == null ? DEFAULT : s;
    }

}
