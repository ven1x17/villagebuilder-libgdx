package com.badlogic.test.core.domain.hud;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.test.core.util.Settings;

public class Hud extends Stage {

    public Minimap minimap;
    public Stage textStage;
    public final CenteredHudLabel globalTooltip;

    public Hud(SpriteBatch spriteBatch) {
        super(new FitViewport(Settings.w, Settings.h), spriteBatch);
        textStage = new Stage(new FitViewport(Settings.w, Settings.h), spriteBatch);
        minimap = new Minimap();
        addActor(minimap);
        globalTooltip = new CenteredHudLabel(null, 24, Settings.w / 2f, 50);
        globalTooltip.centerAtPosition();
        addLabel(globalTooltip);
    }

    public void addLabel(Label label) {
        textStage.addActor(label);
    }

    public void update() {
        getBatch().setProjectionMatrix(getCamera().combined);
        act();
        draw();
        textStage.act();
        textStage.draw();
    }
}
