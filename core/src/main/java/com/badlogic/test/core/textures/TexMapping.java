package com.badlogic.test.core.textures;

public enum TexMapping {
    MAIN_ACTOR("main_actor.png"),
    MOVEMENT_TARGET_CROSS("move_cross.png"),
    GROUND_BASE("crappy_ground_base.png"),
    GROUND_0("crappy_ground_0.png"),
    GROUND_1("crappy_ground_1.png"),
    GROUND_2("crappy_ground_2.png"),
    TREE_APPLE("tree_apple.png"),
    TREE_OAK("tree_oak.png"),
    TREE_PINE("tree_pine.png"),
    GROUND_4("crappy_ground_4.png"),
    STONE("rock.png"),
    LAKE_WATER("lake_water_base.png");

    public String path;

    TexMapping(String path) {
        this.path = path;
    }
}
